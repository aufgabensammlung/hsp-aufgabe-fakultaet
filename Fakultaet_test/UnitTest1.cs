using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fakultaet;

namespace Fakultaet_test
{
    [TestClass]
    public class Fakultaet_test
    {
        [TestMethod]
        public void Fakultaet_Test3()
        {
            Fakultaet_x prg = new Fakultaet_x();

            var fakultaet = prg.Fakultaet_n(3);

            Assert.AreEqual(6, fakultaet);

        }

        [TestMethod]
        public void Fakultaet_Test4()
        {
            Fakultaet_x prg = new Fakultaet_x();

            var fakultaet = prg.Fakultaet_n(5);

            Assert.AreEqual(120, fakultaet);

        }
    }
}
