﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakultaet
{
    public class Fakultaet_x
    {
        public int Fakultaet_n(int n)
        {
            int fakultaet = 1;

            // Impementieren Sie hier die Berechnung der Fakultät als:
            // 1. for-Schleife
            // 2. while-Schleife

            // Stretch goals:
            // 3. Rekursion
            // 4. Schreiben Sie das Programm um auf double- Fliesskommazahlen
            // 5. Ergänzen Sie die Behandlung von negativen Eingaben
            //      (z.B. durch die Überprüfung der Eingabe)
            //      (z.B. Berechung ergibt Not a Number: Double.NaN)
            // 6. Verbessern Sie die Unit-Tests mit einem Test für negative Zahlen
            //      (Abfrage: Double.IsNaN)

            return fakultaet;
        }

        static void Main(string[] args)
        {
            // Einstiegspunkt für Ihr Programm
            Fakultaet_x prg = new Fakultaet_x();

            // Eingabe
            Console.WriteLine("Zu welcher Zahl möchtest du die Fakultaet ausgeben?");
            Console.Write("Eingabe: ");
            int n = Convert.ToInt32(Console.ReadLine());

            // Berechnung
            int fakultaet = prg.Fakultaet_n(n);

            // Ausgabe
            Console.WriteLine(fakultaet);
            Console.ReadKey();

        }
    }
}
